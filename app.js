const order = {
    toppings: [],
    basePrice: 80,
    total: 80
}

const toppings = [{
        name: 'pepperoni',
        price: 20
    }, {
        name: 'mushroom',
        price: 40
    }, {
        name: 'cheese',
        price: 0
    }, {
        name: 'pineapple',
        price: 30
    }, {
        name: 'pepperoniSpicy',
        price: 4000
    }
]

// Cache topping containers
let $pizza  = null;
let $mushrooms = null;
let $pepperoni = null;
let $pineapple = null;
let $total = null;
//Ekstra Spicy
let $pepperoniSpicy = null;

$(document).ready(function () {
    // Write the best jQuery ever here
    $pizza =  $(".pizza");
    $mushrooms = $(".mushrooms");
    $pepperoni = $(".pepperonis");
    $pineapple = $(".pineapples");
    $total = $("#total");
    $pepperoniSpicy = $(".pepperoniSpicies")

    order.toppings.push(toppings[2].name);

    $("#cheese").click(function(e){
        $($pizza).toggleClass("no-cheese");
        if($($pizza).hasClass("pizza no-cheese")){
            order.toppings = jQuery.grep(order.toppings, function(value){
                return value != 'cheese';
            })
        }
        else{
            order.toppings.push(toppings[2].name);
        }
    })

    $("#pepperoni").click(function(e){

        if($($pepperoni).children().length > 0){
            $($pepperoni).empty();
            order.toppings = jQuery.grep(order.toppings, function(value){
                return value != 'pepperoni';
            })
            order.total -= toppings[0].price;
            $($total).text(order.total + " NOK");
        }
        else{
            $($pepperoni).append(generateTopping(e.target.id));
            order.toppings.push(toppings[0].name);
            order.total += toppings[0].price;
            $($total).text(order.total + " NOK");
        }
    })

    $("#mushroom").click(function(e){

        if($($mushrooms).children().length > 0){
            $($mushrooms).empty();
            order.toppings = jQuery.grep(order.toppings, function(value){
                return value != 'mushroom';
            })
            order.total -= toppings[1].price;
            $($total).text(order.total + " NOK");
        }
        else{
            $($mushrooms).append(generateTopping(e.target.id));
            order.toppings.push(toppings[1].name);
            order.total += toppings[1].price;
            $($total).text(order.total + " NOK");
        }
    })

    $("#pineapple").click(function(e){

        if($($pineapple).children().length > 0){
            $($pineapple).empty();
            order.toppings = jQuery.grep(order.toppings, function(value){
                return value != 'pineapple';
            })
            order.total -= toppings[3].price;
            $($total).text(order.total + " NOK");
        }
        else{
            $($pineapple).append(generateTopping(e.target.id));
            order.toppings.push(toppings[3].name);
            order.total += toppings[3].price;
            $($total).text(order.total + " NOK");
        }
    })

    //Random extra memes I added to the task, the task itself is everything above

    $("#pepperoniSpicy").click(function(e){
        if($($pepperoniSpicy).children().length > 0){
            $($pepperoniSpicy).empty();
            order.total -= toppings[4].price;
            $($total).text(order.total + " NOK");
        }
        else{
            $($pepperoniSpicy).append(generateTopping(e.target.id));
            console.log(e.target.id);
            
            order.total += toppings[4].price;
            $($total).text(order.total + " ZAR");
        }
    })

    $("#memes").click(function(e){
        window.location.href = "meme.html";
    })

    // $("#boring").click(function(e){a
    //     window.location.href = "boring.html";
    // })

    // $("#showBoring").click(function(e){
    //     $("#changeHead").text("Sike! AGAIN! HAHAHAHHAHA");
    //     $(".images").toggle();
    // })
});

// Automatically generate the toppings based on the name and id from the button.
function generateTopping(topping) {
    const looper = Array(10).fill(topping);
    return looper.map(item => `<div class="${item}"></div>`).join('');
}