# Made by Lene and Eddie

Pretty straight forward task. 
Used jquery to assign a function to each button and that was pretty much it.

Since cheese has no price, the method was much simpler than the others, as there is no need to add 0 NOK price to the total.
Only thing that really happens is that we toggle between the "pizza" class and the "pizza no-cheese" class.

As a little fun, added a "make it spicy" button, for the memes.